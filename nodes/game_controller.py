#!/usr/bin/env python

import rospy
from std_msgs.msg import String
import json
from std_srvs.srv import Trigger, TriggerResponse
from gazebo_msgs.srv import GetModelState, GetPhysicsProperties
import time
import os
import requests

class GameController:

    def __init__(self, robot_name):

        self.robot_name = robot_name

        self.robot_status = "pause"
        self.game_message = "mission_running"

        self.robot_pose_x = 0
        self.robot_pose_y = 0

        self.finished = False
        self.start_flag = False
        self.timeout = 120

        self.metrics = {
            'robot_name' : self.robot_name,
            'robot_status' : self.robot_status,
            'game_message' : self.game_message,
        }

        rospy.init_node("game_controller")

        while not self.get_pause():
            pass

        self.metric_pub = rospy.Publisher('simulation_metrics', String, queue_size=10)

        s = rospy.Service('/robot_handler', Trigger, self.handle_robot)

        try:
            rospy.wait_for_service("/gazebo/get_model_state", 1.0)
            self.robot_check = rospy.ServiceProxy("/gazebo/get_model_state", GetModelState)

            resp = self.robot_check("linerider", "")
            if not resp.success == True:
                print("no robot around")
                self.robot_status = "no_robot"
                self.game_message = "mission_stopped"

        except (rospy.ServiceException, rospy.ROSException), e:
            print("no robot around")
            self.robot_status = "no_robot"
            self.game_message = "mission_stopped"

            rospy.logerr("Service call failed: %s" % (e,))

        rate = rospy.Rate(5)

        while ~rospy.is_shutdown():
            self.check_robot_stats()
            if not self.finished:
                self.now = rospy.get_time()

            if self.finished and self.now < self.timeout:
                self.achieve(79)

            self.publish_metrics()
            rate.sleep()

    def get_pause(self):
        try:
            rospy.wait_for_service("/gazebo/get_physics_properties", 1.0)
            self.phy_prop = rospy.ServiceProxy("/gazebo/get_physics_properties", GetPhysicsProperties)

            resp = self.phy_prop()
            return resp.pause

        except (rospy.ServiceException, rospy.ROSException), e:
            rospy.logerr("Service call failed: %s" % (e,))

    def check_robot_stats(self):
            resp = self.robot_check("linerider", "")
            self.robot_pose_x = resp.pose.position.x
            self.robot_pose_y = resp.pose.position.y

            # first straight
            if abs(self.robot_pose_y) < 2 and abs(self.robot_pose_x) < 2:
                if self.start_flag:
                    self.finished = True
            else:
                self.start_flag = True
    def handle_robot(self,req):
        if self.robot_status == "pause":
            time.sleep(6)
            self.robot_status = "run"
            return TriggerResponse(
                success = True,
                message = self.robot_status
            )

        elif self.robot_status == "run":
            return TriggerResponse(
                success = True,
                message = self.robot_status
            )

        elif self.robot_status == "stop":
            return TriggerResponse(
                success = True,
                message = self.robot_status
            )

        elif self.robot_status == "no_robot":
            return TriggerResponse(
                success = True,
                message = self.robot_status
            )

    def publish_metrics(self):

        self.metrics['status'] = str(self.robot_status)
        self.metrics['robot_status'] = str(self.robot_status)
        self.metrics['time'] = str(self.now)

        self.metric_pub.publish(json.dumps(self.metrics, sort_keys=True))

    def achieve(self, achievement_id):
        # get parameters from environment
        host = os.environ.get('RIDERS_HOST', None)
        project_id = os.environ.get('RIDERS_PROJECT_ID', None)
        token = os.environ.get('RIDERS_AUTH_TOKEN', None)

        # generate request url
        achievement_url = '%s/api/v1/project/%s/achievements/%s/achieve/' % (host, project_id, achievement_id)

        # send request
        rqst = requests.post(achievement_url, headers={
            'Authorization': 'Token %s' % token,
            'Content-Type': 'application/json',
        })


if __name__ == '__main__':
    # Name of robot
    controller = GameController("linerider")
